package com.iman.weather;

import org.junit.Test;

import com.iman.weather.data.WeatherData;

import static org.junit.Assert.*;

public class WeatherDataUnitTest {
    public void handleJSON(String json) throws Exception {
        WeatherData weatherData = WeatherData.getWeatherData(json);

        assertNotNull("WeatherData", weatherData);
        System.out.print(weatherData.toString());
        System.out.print(weatherData.currently.toString());
        System.out.print(weatherData.minutely.toString());
        System.out.print(weatherData.hourly.toString());
        System.out.print(weatherData.daily.toString());
        System.out.print(weatherData.alertsToString());
        System.out.print(weatherData.flags.toString());
    }

    @Test
    public void testWeatherData() throws Exception{
        String json = "{\"latitude\":37.8267,\"longitude\":-122.423,\"timezone\":\"America/Los_Angeles\",\"offset\":-7,\"currently\":{\"time\":1464321743," +
                      "\"summary\":\"Partly Cloudy\",\"icon\":\"partly-cloudy-night\",\"nearestStormDistance\":23,\"nearestStormBearing\":39," +
                      "\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":56,\"apparentTemperature\":56,\"dewPoint\":50.05,\"humidity\":0.8," +
                      "\"windSpeed\":9.42,\"windBearing\":270,\"visibility\":7.61,\"cloudCover\":0.26,\"pressure\":1015.39,\"ozone\":351.73}," +
                      "\"minutely\":{\"summary\":\"Partly cloudy for the hour.\",\"icon\":\"partly-cloudy-night\",\"data\":[{\"time\":1464321720," +
                      "\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464321780,\"precipIntensity\":0,\"precipProbability\":0}," +
                      "{\"time\":1464321840,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464321900,\"precipIntensity\":0," +
                      "\"precipProbability\":0},{\"time\":1464321960,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464322020," +
                      "\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464322080,\"precipIntensity\":0,\"precipProbability\":0}," +
                      "{\"time\":1464322140,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464322200,\"precipIntensity\":0," +
                      "\"precipProbability\":0},{\"time\":1464322260,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464322320," +
                      "\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464322380,\"precipIntensity\":0,\"precipProbability\":0}," +
                      "{\"time\":1464322440,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464322500,\"precipIntensity\":0," +
                      "\"precipProbability\":0},{\"time\":1464322560,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464322620," +
                      "\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464322680,\"precipIntensity\":0,\"precipProbability\":0}," +
                      "{\"time\":1464322740,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464322800,\"precipIntensity\":0," +
                      "\"precipProbability\":0},{\"time\":1464322860,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464322920," +
                      "\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464322980,\"precipIntensity\":0,\"precipProbability\":0}," +
                      "{\"time\":1464323040,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464323100,\"precipIntensity\":0," +
                      "\"precipProbability\":0},{\"time\":1464323160,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464323220," +
                      "\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464323280,\"precipIntensity\":0,\"precipProbability\":0}," +
                      "{\"time\":1464323340,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464323400,\"precipIntensity\":0," +
                      "\"precipProbability\":0},{\"time\":1464323460,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464323520," +
                      "\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464323580,\"precipIntensity\":0,\"precipProbability\":0}," +
                      "{\"time\":1464323640,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464323700,\"precipIntensity\":0," +
                      "\"precipProbability\":0},{\"time\":1464323760,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464323820," +
                      "\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464323880,\"precipIntensity\":0,\"precipProbability\":0}," +
                      "{\"time\":1464323940,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464324000,\"precipIntensity\":0," +
                      "\"precipProbability\":0},{\"time\":1464324060,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464324120," +
                      "\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464324180,\"precipIntensity\":0,\"precipProbability\":0}," +
                      "{\"time\":1464324240,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464324300,\"precipIntensity\":0," +
                      "\"precipProbability\":0},{\"time\":1464324360,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464324420," +
                      "\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464324480,\"precipIntensity\":0,\"precipProbability\":0}," +
                      "{\"time\":1464324540,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464324600,\"precipIntensity\":0," +
                      "\"precipProbability\":0},{\"time\":1464324660,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464324720," +
                      "\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464324780,\"precipIntensity\":0,\"precipProbability\":0}," +
                      "{\"time\":1464324840,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464324900,\"precipIntensity\":0," +
                      "\"precipProbability\":0},{\"time\":1464324960,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464325020," +
                      "\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464325080,\"precipIntensity\":0,\"precipProbability\":0}," +
                      "{\"time\":1464325140,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464325200,\"precipIntensity\":0," +
                      "\"precipProbability\":0},{\"time\":1464325260,\"precipIntensity\":0,\"precipProbability\":0},{\"time\":1464325320," +
                      "\"precipIntensity\":0,\"precipProbability\":0}]},\"hourly\":{\"summary\":\"Partly cloudy until tomorrow morning.\"," +
                      "\"icon\":\"partly-cloudy-night\",\"data\":[{\"time\":1464321600,\"summary\":\"Partly Cloudy\",\"icon\":\"partly-cloudy-night\"," +
                      "\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":56,\"apparentTemperature\":56,\"dewPoint\":50.03,\"humidity\":0.8," +
                      "\"windSpeed\":9.47,\"windBearing\":270,\"visibility\":7.61,\"cloudCover\":0.26,\"pressure\":1015.38,\"ozone\":351.72}," +
                      "{\"time\":1464325200,\"summary\":\"Partly Cloudy\",\"icon\":\"partly-cloudy-night\",\"precipIntensity\":0,\"precipProbability\":0," +
                      "\"temperature\":56.07,\"apparentTemperature\":56.07,\"dewPoint\":50.62,\"humidity\":0.82,\"windSpeed\":8.17,\"windBearing\":275," +
                      "\"visibility\":7.56,\"cloudCover\":0.26,\"pressure\":1015.42,\"ozone\":351.96},{\"time\":1464328800,\"summary\":\"Partly Cloudy\"," +
                      "\"icon\":\"partly-cloudy-night\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":55.59,\"apparentTemperature\":55.59," +
                      "\"dewPoint\":50.58,\"humidity\":0.83,\"windSpeed\":6.86,\"windBearing\":274,\"visibility\":7.55,\"cloudCover\":0.36,\"pressure\":1015" +
                      ".39,\"ozone\":351.88},{\"time\":1464332400,\"summary\":\"Partly Cloudy\",\"icon\":\"partly-cloudy-night\",\"precipIntensity\":0," +
                      "\"precipProbability\":0,\"temperature\":55.35,\"apparentTemperature\":55.35,\"dewPoint\":50.56,\"humidity\":0.84,\"windSpeed\":5.82," +
                      "\"windBearing\":276,\"visibility\":7.72,\"cloudCover\":0.37,\"pressure\":1015.24,\"ozone\":351.29},{\"time\":1464336000," +
                      "\"summary\":\"Partly Cloudy\",\"icon\":\"partly-cloudy-night\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":55.29," +
                      "\"apparentTemperature\":55.29,\"dewPoint\":50.56,\"humidity\":0.84,\"windSpeed\":4.76,\"windBearing\":275,\"visibility\":7.81," +
                      "\"cloudCover\":0.56,\"pressure\":1015.04,\"ozone\":350.38},{\"time\":1464339600,\"summary\":\"Partly Cloudy\"," +
                      "\"icon\":\"partly-cloudy-night\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":54.61,\"apparentTemperature\":54.61," +
                      "\"dewPoint\":50.09,\"humidity\":0.85,\"windSpeed\":4.2,\"windBearing\":277,\"visibility\":7.97,\"cloudCover\":0.58,\"pressure\":1014" +
                      ".81,\"ozone\":349.5},{\"time\":1464343200,\"summary\":\"Mostly Cloudy\",\"icon\":\"partly-cloudy-night\",\"precipIntensity\":0," +
                      "\"precipProbability\":0,\"temperature\":53.96,\"apparentTemperature\":53.96,\"dewPoint\":49.53,\"humidity\":0.85,\"windSpeed\":3.65," +
                      "\"windBearing\":287,\"visibility\":8.02,\"cloudCover\":0.6,\"pressure\":1014.48,\"ozone\":348.74},{\"time\":1464346800," +
                      "\"summary\":\"Mostly Cloudy\",\"icon\":\"partly-cloudy-night\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":53.24," +
                      "\"apparentTemperature\":53.24,\"dewPoint\":48.86,\"humidity\":0.85,\"windSpeed\":3.72,\"windBearing\":299,\"visibility\":8.24," +
                      "\"cloudCover\":0.61,\"pressure\":1014.12,\"ozone\":348.02},{\"time\":1464350400,\"summary\":\"Mostly Cloudy\"," +
                      "\"icon\":\"partly-cloudy-night\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":52.79,\"apparentTemperature\":52.79," +
                      "\"dewPoint\":48.17,\"humidity\":0.84,\"windSpeed\":3.46,\"windBearing\":309,\"visibility\":8.44,\"cloudCover\":0.66,\"pressure\":1013" +
                      ".92,\"ozone\":347.41},{\"time\":1464354000,\"summary\":\"Mostly Cloudy\",\"icon\":\"partly-cloudy-day\",\"precipIntensity\":0," +
                      "\"precipProbability\":0,\"temperature\":53.74,\"apparentTemperature\":53.74,\"dewPoint\":48.79,\"humidity\":0.83,\"windSpeed\":3.47," +
                      "\"windBearing\":314,\"visibility\":8.67,\"cloudCover\":0.62,\"pressure\":1014.01,\"ozone\":346.98},{\"time\":1464357600," +
                      "\"summary\":\"Partly Cloudy\",\"icon\":\"partly-cloudy-day\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":56.1," +
                      "\"apparentTemperature\":56.1,\"dewPoint\":50.23,\"humidity\":0.81,\"windSpeed\":3.55,\"windBearing\":315,\"visibility\":9.13," +
                      "\"cloudCover\":0.56,\"pressure\":1014.25,\"ozone\":346.66},{\"time\":1464361200,\"summary\":\"Partly Cloudy\"," +
                      "\"icon\":\"partly-cloudy-day\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":59.04,\"apparentTemperature\":59.04," +
                      "\"dewPoint\":50.65,\"humidity\":0.74,\"windSpeed\":3.47,\"windBearing\":315,\"visibility\":9.83,\"cloudCover\":0.49,\"pressure\":1014" +
                      ".42,\"ozone\":346.4},{\"time\":1464364800,\"summary\":\"Partly Cloudy\",\"icon\":\"partly-cloudy-day\",\"precipIntensity\":0," +
                      "\"precipProbability\":0,\"temperature\":60.89,\"apparentTemperature\":60.89,\"dewPoint\":50.29,\"humidity\":0.68,\"windSpeed\":3.91," +
                      "\"windBearing\":313,\"visibility\":9.98,\"cloudCover\":0.44,\"pressure\":1014.44,\"ozone\":346.24},{\"time\":1464368400," +
                      "\"summary\":\"Clear\",\"icon\":\"clear-day\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":63.08," +
                      "\"apparentTemperature\":63.08,\"dewPoint\":50.1,\"humidity\":0.63,\"windSpeed\":4.82,\"windBearing\":309,\"visibility\":10," +
                      "\"cloudCover\":0.23,\"pressure\":1014.38,\"ozone\":346.14},{\"time\":1464372000,\"summary\":\"Clear\",\"icon\":\"clear-day\"," +
                      "\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":65.58,\"apparentTemperature\":65.58,\"dewPoint\":50.08,\"humidity\":0" +
                      ".57,\"windSpeed\":5.77,\"windBearing\":306,\"visibility\":10,\"cloudCover\":0.08,\"pressure\":1014.25,\"ozone\":345.86}," +
                      "{\"time\":1464375600,\"summary\":\"Clear\",\"icon\":\"clear-day\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":68.66," +
                      "\"apparentTemperature\":68.66,\"dewPoint\":51.08,\"humidity\":0.53,\"windSpeed\":6.98,\"windBearing\":299,\"visibility\":10," +
                      "\"cloudCover\":0.08,\"pressure\":1014.06,\"ozone\":345.2},{\"time\":1464379200,\"summary\":\"Clear\",\"icon\":\"clear-day\"," +
                      "\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":72.18,\"apparentTemperature\":72.18,\"dewPoint\":52.97,\"humidity\":0" +
                      ".51,\"windSpeed\":8.34,\"windBearing\":294,\"visibility\":10,\"cloudCover\":0.09,\"pressure\":1013.78,\"ozone\":344.37}," +
                      "{\"time\":1464382800,\"summary\":\"Clear\",\"icon\":\"clear-day\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":74.26," +
                      "\"apparentTemperature\":74.26,\"dewPoint\":55.29,\"humidity\":0.52,\"windSpeed\":8.98,\"windBearing\":290,\"visibility\":10," +
                      "\"cloudCover\":0.1,\"pressure\":1013.42,\"ozone\":343.78},{\"time\":1464386400,\"summary\":\"Clear\",\"icon\":\"clear-day\"," +
                      "\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":75.45,\"apparentTemperature\":75.45,\"dewPoint\":56.84,\"humidity\":0" +
                      ".52,\"windSpeed\":9.69,\"windBearing\":290,\"visibility\":10,\"cloudCover\":0.15,\"pressure\":1012.9,\"ozone\":343.45}," +
                      "{\"time\":1464390000,\"summary\":\"Clear\",\"icon\":\"clear-day\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":74.22," +
                      "\"apparentTemperature\":74.22,\"dewPoint\":56.16,\"humidity\":0.53,\"windSpeed\":10.51,\"windBearing\":293,\"visibility\":10," +
                      "\"cloudCover\":0.16,\"pressure\":1012.29,\"ozone\":343.36},{\"time\":1464393600,\"summary\":\"Clear\",\"icon\":\"clear-day\"," +
                      "\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":72.54,\"apparentTemperature\":72.54,\"dewPoint\":55.31,\"humidity\":0" +
                      ".55,\"windSpeed\":10.84,\"windBearing\":295,\"visibility\":10,\"cloudCover\":0.17,\"pressure\":1011.75,\"ozone\":343.84}," +
                      "{\"time\":1464397200,\"summary\":\"Clear\",\"icon\":\"clear-day\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":69.86," +
                      "\"apparentTemperature\":69.86,\"dewPoint\":54.3,\"humidity\":0.58,\"windSpeed\":10.28,\"windBearing\":295,\"visibility\":10," +
                      "\"cloudCover\":0.22,\"pressure\":1011.29,\"ozone\":345.26},{\"time\":1464400800,\"summary\":\"Clear\",\"icon\":\"clear-day\"," +
                      "\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":67.12,\"apparentTemperature\":67.12,\"dewPoint\":53.21,\"humidity\":0" +
                      ".61,\"windSpeed\":9.25,\"windBearing\":299,\"visibility\":10,\"cloudCover\":0.21,\"pressure\":1010.91,\"ozone\":347.26}," +
                      "{\"time\":1464404400,\"summary\":\"Clear\",\"icon\":\"clear-day\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":65.2," +
                      "\"apparentTemperature\":65.2,\"dewPoint\":53.13,\"humidity\":0.65,\"windSpeed\":8.29,\"windBearing\":302,\"visibility\":10," +
                      "\"cloudCover\":0.14,\"pressure\":1010.73,\"ozone\":349.1},{\"time\":1464408000,\"summary\":\"Clear\",\"icon\":\"clear-night\"," +
                      "\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":63.32,\"apparentTemperature\":63.32,\"dewPoint\":53.1,\"humidity\":0.69," +
                      "\"windSpeed\":7.6,\"windBearing\":306,\"visibility\":10,\"cloudCover\":0.08,\"pressure\":1010.87,\"ozone\":350.51}," +
                      "{\"time\":1464411600,\"summary\":\"Clear\",\"icon\":\"clear-night\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":61" +
                      ".85,\"apparentTemperature\":61.85,\"dewPoint\":52.48,\"humidity\":0.71,\"windSpeed\":7.09,\"windBearing\":310,\"visibility\":10," +
                      "\"cloudCover\":0.09,\"pressure\":1011.2,\"ozone\":351.77},{\"time\":1464415200,\"summary\":\"Clear\",\"icon\":\"clear-night\"," +
                      "\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":60.83,\"apparentTemperature\":60.83,\"dewPoint\":52.14,\"humidity\":0" +
                      ".73,\"windSpeed\":6.72,\"windBearing\":313,\"visibility\":10,\"cloudCover\":0.12,\"pressure\":1011.4,\"ozone\":352.97}," +
                      "{\"time\":1464418800,\"summary\":\"Clear\",\"icon\":\"clear-night\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":60" +
                      ".14,\"apparentTemperature\":60.14,\"dewPoint\":51.98,\"humidity\":0.74,\"windSpeed\":6.33,\"windBearing\":314,\"visibility\":10," +
                      "\"cloudCover\":0.14,\"pressure\":1011.37,\"ozone\":354.1},{\"time\":1464422400,\"summary\":\"Clear\",\"icon\":\"clear-night\"," +
                      "\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":59.55,\"apparentTemperature\":59.55,\"dewPoint\":51.79,\"humidity\":0" +
                      ".75,\"windSpeed\":6.05,\"windBearing\":314,\"visibility\":10,\"cloudCover\":0.17,\"pressure\":1011.23,\"ozone\":355.17}," +
                      "{\"time\":1464426000,\"summary\":\"Clear\",\"icon\":\"clear-night\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":58" +
                      ".9,\"apparentTemperature\":58.9,\"dewPoint\":51.58,\"humidity\":0.77,\"windSpeed\":5.86,\"windBearing\":313,\"visibility\":10," +
                      "\"cloudCover\":0.2,\"pressure\":1011.07,\"ozone\":356.27},{\"time\":1464429600,\"summary\":\"Clear\",\"icon\":\"clear-night\"," +
                      "\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":58.04,\"apparentTemperature\":58.04,\"dewPoint\":51.16,\"humidity\":0" +
                      ".78,\"windSpeed\":5.83,\"windBearing\":311,\"visibility\":10,\"cloudCover\":0.22,\"pressure\":1010.86,\"ozone\":357.49}," +
                      "{\"time\":1464433200,\"summary\":\"Clear\",\"icon\":\"clear-night\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":57" +
                      ".12,\"apparentTemperature\":57.12,\"dewPoint\":50.61,\"humidity\":0.79,\"windSpeed\":5.91,\"windBearing\":309,\"visibility\":10," +
                      "\"cloudCover\":0.24,\"pressure\":1010.62,\"ozone\":358.74},{\"time\":1464436800,\"summary\":\"Partly Cloudy\"," +
                      "\"icon\":\"partly-cloudy-night\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":56.52,\"apparentTemperature\":56.52," +
                      "\"dewPoint\":50,\"humidity\":0.79,\"windSpeed\":5.82,\"windBearing\":307,\"visibility\":10,\"cloudCover\":0.25,\"pressure\":1010.5," +
                      "\"ozone\":359.9},{\"time\":1464440400,\"summary\":\"Partly Cloudy\",\"icon\":\"partly-cloudy-day\",\"precipIntensity\":0," +
                      "\"precipProbability\":0,\"temperature\":57.57,\"apparentTemperature\":57.57,\"dewPoint\":50.49,\"humidity\":0.77,\"windSpeed\":5.14," +
                      "\"windBearing\":306,\"visibility\":10,\"cloudCover\":0.27,\"pressure\":1010.61,\"ozone\":360.93},{\"time\":1464444000," +
                      "\"summary\":\"Partly Cloudy\",\"icon\":\"partly-cloudy-day\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":58.91," +
                      "\"apparentTemperature\":58.91,\"dewPoint\":50.84,\"humidity\":0.75,\"windSpeed\":4.19,\"windBearing\":307,\"visibility\":10," +
                      "\"cloudCover\":0.29,\"pressure\":1010.85,\"ozone\":361.88},{\"time\":1464447600,\"summary\":\"Partly Cloudy\"," +
                      "\"icon\":\"partly-cloudy-day\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":60.3,\"apparentTemperature\":60.3," +
                      "\"dewPoint\":50.81,\"humidity\":0.71,\"windSpeed\":3.62,\"windBearing\":309,\"visibility\":10,\"cloudCover\":0.29,\"pressure\":1011" +
                      ".09,\"ozone\":362.78},{\"time\":1464451200,\"summary\":\"Partly Cloudy\",\"icon\":\"partly-cloudy-day\",\"precipIntensity\":0," +
                      "\"precipProbability\":0,\"temperature\":62.05,\"apparentTemperature\":62.05,\"dewPoint\":50.4,\"humidity\":0.66,\"windSpeed\":3.63," +
                      "\"windBearing\":314,\"visibility\":10,\"cloudCover\":0.27,\"pressure\":1011.24,\"ozone\":363.68},{\"time\":1464454800," +
                      "\"summary\":\"Clear\",\"icon\":\"clear-day\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":64.43," +
                      "\"apparentTemperature\":64.43,\"dewPoint\":49.99,\"humidity\":0.59,\"windSpeed\":3.89,\"windBearing\":318,\"visibility\":10," +
                      "\"cloudCover\":0.22,\"pressure\":1011.36,\"ozone\":364.53},{\"time\":1464458400,\"summary\":\"Clear\",\"icon\":\"clear-day\"," +
                      "\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":67.07,\"apparentTemperature\":67.07,\"dewPoint\":50.2,\"humidity\":0.55," +
                      "\"windSpeed\":4.33,\"windBearing\":314,\"visibility\":10,\"cloudCover\":0.17,\"pressure\":1011.45,\"ozone\":365.25}," +
                      "{\"time\":1464462000,\"summary\":\"Clear\",\"icon\":\"clear-day\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":69.76," +
                      "\"apparentTemperature\":69.76,\"dewPoint\":51.41,\"humidity\":0.52,\"windSpeed\":5.21,\"windBearing\":302,\"visibility\":10," +
                      "\"cloudCover\":0.13,\"pressure\":1011.52,\"ozone\":365.8},{\"time\":1464465600,\"summary\":\"Clear\",\"icon\":\"clear-day\"," +
                      "\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":72.72,\"apparentTemperature\":72.72,\"dewPoint\":53.49,\"humidity\":0" +
                      ".51,\"windSpeed\":6.74,\"windBearing\":290,\"visibility\":10,\"cloudCover\":0.09,\"pressure\":1011.56,\"ozone\":366.2}," +
                      "{\"time\":1464469200,\"summary\":\"Clear\",\"icon\":\"clear-day\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":75.2," +
                      "\"apparentTemperature\":75.2,\"dewPoint\":55.65,\"humidity\":0.51,\"windSpeed\":8.12,\"windBearing\":285,\"visibility\":10," +
                      "\"cloudCover\":0.06,\"pressure\":1011.52,\"ozone\":366.42},{\"time\":1464472800,\"summary\":\"Clear\",\"icon\":\"clear-day\"," +
                      "\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":76.54,\"apparentTemperature\":76.54,\"dewPoint\":57.22,\"humidity\":0" +
                      ".51,\"windSpeed\":9.35,\"windBearing\":284,\"visibility\":10,\"cloudCover\":0.07,\"pressure\":1011.32,\"ozone\":366.43}," +
                      "{\"time\":1464476400,\"summary\":\"Clear\",\"icon\":\"clear-day\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":75.57," +
                      "\"apparentTemperature\":75.57,\"dewPoint\":57.19,\"humidity\":0.53,\"windSpeed\":10.49,\"windBearing\":285,\"visibility\":10," +
                      "\"cloudCover\":0.11,\"pressure\":1011.02,\"ozone\":366.26},{\"time\":1464480000,\"summary\":\"Clear\",\"icon\":\"clear-day\"," +
                      "\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":73.48,\"apparentTemperature\":73.48,\"dewPoint\":56.61,\"humidity\":0" +
                      ".56,\"windSpeed\":10.93,\"windBearing\":284,\"visibility\":10,\"cloudCover\":0.12,\"pressure\":1010.84,\"ozone\":365.95}," +
                      "{\"time\":1464483600,\"summary\":\"Clear\",\"icon\":\"clear-day\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":70.61," +
                      "\"apparentTemperature\":70.61,\"dewPoint\":55.93,\"humidity\":0.6,\"windSpeed\":10.3,\"windBearing\":284,\"visibility\":10," +
                      "\"cloudCover\":0.11,\"pressure\":1010.83,\"ozone\":365.44},{\"time\":1464487200,\"summary\":\"Clear\",\"icon\":\"clear-day\"," +
                      "\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":67.45,\"apparentTemperature\":67.45,\"dewPoint\":55.26,\"humidity\":0" +
                      ".65,\"windSpeed\":8.86,\"windBearing\":284,\"visibility\":10,\"cloudCover\":0.08,\"pressure\":1010.94,\"ozone\":364.79}," +
                      "{\"time\":1464490800,\"summary\":\"Clear\",\"icon\":\"clear-day\",\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":64.68," +
                      "\"apparentTemperature\":64.68,\"dewPoint\":54.63,\"humidity\":0.7,\"windSpeed\":7.63,\"windBearing\":284,\"visibility\":10," +
                      "\"cloudCover\":0.06,\"pressure\":1011.15,\"ozone\":364.28},{\"time\":1464494400,\"summary\":\"Clear\",\"icon\":\"clear-night\"," +
                      "\"precipIntensity\":0,\"precipProbability\":0,\"temperature\":62.59,\"apparentTemperature\":62.59,\"dewPoint\":54.23,\"humidity\":0" +
                      ".74,\"windSpeed\":6.71,\"windBearing\":284,\"visibility\":10,\"cloudCover\":0.07,\"pressure\":1011.49,\"ozone\":364.55}]}," +
                      "\"daily\":{\"summary\":\"No precipitation throughout the week, with temperatures rising to 78°F on Monday.\",\"icon\":\"clear-day\"," +
                      "\"data\":[{\"time\":1464246000,\"summary\":\"Mostly cloudy until afternoon.\",\"icon\":\"partly-cloudy-day\"," +
                      "\"sunriseTime\":1464267190,\"sunsetTime\":1464319402,\"moonPhase\":0.66,\"precipIntensity\":0,\"precipIntensityMax\":0," +
                      "\"precipProbability\":0,\"temperatureMin\":53.72,\"temperatureMinTime\":1464260400,\"temperatureMax\":61.82," +
                      "\"temperatureMaxTime\":1464303600,\"apparentTemperatureMin\":53.72,\"apparentTemperatureMinTime\":1464260400," +
                      "\"apparentTemperatureMax\":61.82,\"apparentTemperatureMaxTime\":1464303600,\"dewPoint\":50.98,\"humidity\":0.8,\"windSpeed\":7.63," +
                      "\"windBearing\":262,\"visibility\":9.34,\"cloudCover\":0.44,\"pressure\":1017.72,\"ozone\":356.38},{\"time\":1464332400," +
                      "\"summary\":\"Partly cloudy in the morning.\",\"icon\":\"partly-cloudy-day\",\"sunriseTime\":1464353560,\"sunsetTime\":1464405847," +
                      "\"moonPhase\":0.69,\"precipIntensity\":0,\"precipIntensityMax\":0,\"precipProbability\":0,\"temperatureMin\":52.79," +
                      "\"temperatureMinTime\":1464350400,\"temperatureMax\":75.45,\"temperatureMaxTime\":1464386400,\"apparentTemperatureMin\":52.79," +
                      "\"apparentTemperatureMinTime\":1464350400,\"apparentTemperatureMax\":75.45,\"apparentTemperatureMaxTime\":1464386400,\"dewPoint\":51" +
                      ".83,\"humidity\":0.69,\"windSpeed\":6.35,\"windBearing\":298,\"visibility\":9.41,\"cloudCover\":0.31,\"pressure\":1013.25," +
                      "\"ozone\":347.27},{\"time\":1464418800,\"summary\":\"Partly cloudy in the morning.\",\"icon\":\"partly-cloudy-day\"," +
                      "\"sunriseTime\":1464439931,\"sunsetTime\":1464492291,\"moonPhase\":0.73,\"precipIntensity\":0,\"precipIntensityMax\":0," +
                      "\"precipProbability\":0,\"temperatureMin\":56.52,\"temperatureMinTime\":1464436800,\"temperatureMax\":76.54," +
                      "\"temperatureMaxTime\":1464472800,\"apparentTemperatureMin\":56.52,\"apparentTemperatureMinTime\":1464436800," +
                      "\"apparentTemperatureMax\":76.54,\"apparentTemperatureMaxTime\":1464472800,\"dewPoint\":52.9,\"humidity\":0.67,\"windSpeed\":6.33," +
                      "\"windBearing\":295,\"visibility\":10,\"cloudCover\":0.16,\"pressure\":1011.19,\"ozone\":362.73},{\"time\":1464505200," +
                      "\"summary\":\"Partly cloudy overnight.\",\"icon\":\"partly-cloudy-night\",\"sunriseTime\":1464526304,\"sunsetTime\":1464578735," +
                      "\"moonPhase\":0.76,\"precipIntensity\":0,\"precipIntensityMax\":0,\"precipProbability\":0,\"temperatureMin\":55.67," +
                      "\"temperatureMinTime\":1464523200,\"temperatureMax\":74.28,\"temperatureMaxTime\":1464562800,\"apparentTemperatureMin\":55.67," +
                      "\"apparentTemperatureMinTime\":1464523200,\"apparentTemperatureMax\":74.28,\"apparentTemperatureMaxTime\":1464562800,\"dewPoint\":55" +
                      ".15,\"humidity\":0.76,\"windSpeed\":6.07,\"windBearing\":263,\"visibility\":10,\"cloudCover\":0.17,\"pressure\":1013.99,\"ozone\":344" +
                      ".32},{\"time\":1464591600,\"summary\":\"Partly cloudy in the morning.\",\"icon\":\"partly-cloudy-night\",\"sunriseTime\":1464612678," +
                      "\"sunsetTime\":1464665177,\"moonPhase\":0.8,\"precipIntensity\":0,\"precipIntensityMax\":0,\"precipProbability\":0," +
                      "\"temperatureMin\":55.77,\"temperatureMinTime\":1464606000,\"temperatureMax\":78.01,\"temperatureMaxTime\":1464645600," +
                      "\"apparentTemperatureMin\":55.77,\"apparentTemperatureMinTime\":1464606000,\"apparentTemperatureMax\":78.01," +
                      "\"apparentTemperatureMaxTime\":1464645600,\"dewPoint\":52.91,\"humidity\":0.66,\"windSpeed\":4.93,\"windBearing\":231," +
                      "\"visibility\":10,\"cloudCover\":0.15,\"pressure\":1013.58,\"ozone\":327.37},{\"time\":1464678000,\"summary\":\"Partly cloudy " +
                      "overnight.\",\"icon\":\"partly-cloudy-night\",\"sunriseTime\":1464699054,\"sunsetTime\":1464751618,\"moonPhase\":0.84," +
                      "\"precipIntensity\":0,\"precipIntensityMax\":0,\"precipProbability\":0,\"temperatureMin\":58.06,\"temperatureMinTime\":1464696000," +
                      "\"temperatureMax\":77.14,\"temperatureMaxTime\":1464732000,\"apparentTemperatureMin\":58.06,\"apparentTemperatureMinTime\":1464696000," +
                      "\"apparentTemperatureMax\":77.14,\"apparentTemperatureMaxTime\":1464732000,\"dewPoint\":51.6,\"humidity\":0.62,\"windSpeed\":4.91," +
                      "\"windBearing\":226,\"cloudCover\":0.02,\"pressure\":1010.26,\"ozone\":317.71},{\"time\":1464764400,\"summary\":\"Mostly cloudy " +
                      "throughout the day.\",\"icon\":\"partly-cloudy-day\",\"sunriseTime\":1464785432,\"sunsetTime\":1464838058,\"moonPhase\":0.88," +
                      "\"precipIntensity\":0,\"precipIntensityMax\":0,\"precipProbability\":0,\"temperatureMin\":56.34,\"temperatureMinTime\":1464782400," +
                      "\"temperatureMax\":71.93,\"temperatureMaxTime\":1464822000,\"apparentTemperatureMin\":56.34,\"apparentTemperatureMinTime\":1464782400," +
                      "\"apparentTemperatureMax\":71.93,\"apparentTemperatureMaxTime\":1464822000,\"dewPoint\":51.35,\"humidity\":0.67,\"windSpeed\":5.09," +
                      "\"windBearing\":231,\"cloudCover\":0.69,\"pressure\":1011.33,\"ozone\":318.01},{\"time\":1464850800,\"summary\":\"Overcast throughout " +
                      "the day.\",\"icon\":\"cloudy\",\"sunriseTime\":1464871812,\"sunsetTime\":1464924498,\"moonPhase\":0.91,\"precipIntensity\":0," +
                      "\"precipIntensityMax\":0,\"precipProbability\":0,\"temperatureMin\":56,\"temperatureMinTime\":1464868800,\"temperatureMax\":69.89," +
                      "\"temperatureMaxTime\":1464904800,\"apparentTemperatureMin\":56,\"apparentTemperatureMinTime\":1464868800," +
                      "\"apparentTemperatureMax\":69.89,\"apparentTemperatureMaxTime\":1464904800,\"dewPoint\":49.13,\"humidity\":0.63,\"windSpeed\":5.13," +
                      "\"windBearing\":238,\"cloudCover\":0.97,\"pressure\":1014.94,\"ozone\":314.62}]},\"flags\":{\"sources\":[\"darksky\",\"lamp\",\"gfs\"," +
                      "\"cmc\",\"nam\",\"rap\",\"rtma\",\"sref\",\"fnmoc\",\"isd\",\"nwspa\",\"madis\",\"nearest-precip\"],\"darksky-stations\":[\"KMUX\"]," +
                      "\"lamp-stations\":[\"KAPC\",\"KCCR\",\"KHWD\",\"KLVK\",\"KNUQ\",\"KOAK\",\"KPAO\",\"KSFO\",\"KSQL\"]," +
                      "\"isd-stations\":[\"724943-99999\",\"745039-99999\",\"745065-99999\",\"994016-99999\",\"998479-99999\"],\"madis-stations\":[\"AU915\"," +
                      "\"C5988\",\"C6328\",\"C8158\",\"C9629\",\"CQ147\",\"D5422\",\"E0426\",\"E6067\",\"E9227\",\"FTPC1\",\"GGBC1\",\"OKXC1\",\"PPXC1\"," +
                      "\"PXOC1\",\"SFOC1\"],\"units\":\"us\"}}";

        handleJSON(json);
    }
}