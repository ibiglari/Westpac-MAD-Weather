
package com.iman.weather.data;

import java.util.ArrayList;
import java.util.List;

public class Minutely {

    public String summary;
    public String icon;
    public List<MinutelyDatum> data = new ArrayList<>();

    public String toString() {
        String ret = "Summary: " + summary + "\n" +
                     "Icon: " + icon + "\n";

        for (int i = 0; i < data.size(); i++) {
            MinutelyDatum minute = data.get(i);
            ret += "\n" + minute.toString() + "\n";
        }
        return ret;
    }
}
