
package com.iman.weather.data;

import java.util.ArrayList;
import java.util.List;

public class Flags {

    public List<String> sources         = new ArrayList<>();
    public List<String> darkskyStations = new ArrayList<>();
    public List<String> lampStations    = new ArrayList<>();
    public List<String> isdStations     = new ArrayList<>();
    public List<String> madisStations   = new ArrayList<>();
    public String units;

    public String toString() {
        return "Sources: " + sources.toString() + "\n" +
               "DarkSky Stations: " + darkskyStations.toString() + "\n" +
               "Lamp Stations: " + lampStations.toString() + "\n" +
               "ISD Stations: " + isdStations.toString() + "\n" +
               "MADIS Stations: " + madisStations.toString() + "\n" +
               "Units: " + units;

    }
}
