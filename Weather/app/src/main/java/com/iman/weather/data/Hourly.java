
package com.iman.weather.data;

import java.util.ArrayList;
import java.util.List;

public class Hourly {

    public String summary;
    public String icon;
    public List<HourlyDatum> data = new ArrayList<HourlyDatum>();

    public String toString() {
        String ret = "Summary: " + summary + "\n" +
                     "Icon: " + icon + "\n";

        for (int i = 0; i < data.size(); i++) {
            HourlyDatum hour = data.get(i);
            ret += "\n" + hour.toString() + "\n";
        }
        return ret;
    }
}
