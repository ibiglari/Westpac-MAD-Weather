package com.iman.weather.service;

import android.os.AsyncTask;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;

import com.iman.weather.data.WeatherData;

public class ForecastService extends AsyncTask<Double, Void, String> {
    public interface Callback {
        void onComplete(WeatherData weatherData);

        void onFailure(String reason);
    }

    private WeatherData weatherData;
    private Set<Callback> callbacks = new HashSet<>();

    public ForecastService addObserver(Callback cb) {
        callbacks.add(cb);
        return this;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(Double... params) {
        Thread.currentThread().setName(this.toString());

        String ret = "";

        HttpURLConnection urlConnection = null;
        InputStream       is            = null;
        try {
            URL url;
            url = new URL("https://api.forecast.io/forecast/ab52aa70b1a004d35de892df27ee20b6/" + String.valueOf(params[0]) + "," + String.valueOf
                    (params[1]) + "?units=si");

            urlConnection = (HttpURLConnection) url.openConnection();
            String        result;
            StringBuilder sb = new StringBuilder();

            is = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader br        = new BufferedReader(new InputStreamReader(is));
            String         inputLine;
            while ((inputLine = br.readLine()) != null) {
                sb.append(inputLine);
            }
            result = sb.toString();
            weatherData = WeatherData.getWeatherData(result);
            ret = "OK";
        } catch (Exception e) {
            ret = e.getMessage();
        } finally {
            if (is != null) {
                try {
                    is.close();
                } catch (Exception e) {
                    if (ret.length() > 0)
                        ret += "\n";
                    ret += e.getMessage();
                }
            }
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return ret;
    }

    @Override
    protected void onPostExecute(String result) {
        if (result.equals("OK")) {
            for (Callback cb : callbacks) {
                cb.onComplete(weatherData);
            }
        } else {
            for (Callback cb : callbacks) {
                cb.onFailure(result);
            }
        }
        super.onPostExecute(result);
    }
}
