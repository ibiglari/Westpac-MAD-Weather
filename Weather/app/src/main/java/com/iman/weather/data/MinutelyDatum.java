
package com.iman.weather.data;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MinutelyDatum {

    public long time;
    public double precipIntensity;
    public double precipProbability;

    public String toString() {
        return "Time: " + (new SimpleDateFormat("HH:mm")).format(new Date(time * 1000)) + "\n" +
               "Precipitation Intensity: " + String.valueOf(precipIntensity) + "\n" +
               "Precipitation Probability: " + String.valueOf(precipProbability);
    }
}
