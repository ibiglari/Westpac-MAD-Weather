
package com.iman.weather.data;

import java.util.Date;

public class Currently {

    public long   time;
    public String summary;
    public String icon;
    public double nearestStormDistance;
    public double nearestStormBearing;
    public double precipIntensity;
    public double precipProbability;
    public double temperature;
    public double apparentTemperature;
    public double dewPoint;
    public double humidity;
    public double windSpeed;
    public double windBearing;
    public double visibility;
    public double cloudCover;
    public double pressure;
    public double ozone;

    public String toString() {
        return "Time: " + String.valueOf(new Date(time * 1000)) + "\n" +
               "Summary: " + summary + "\n" +
               "Icon: " + icon + "\n" +
               "Nearest Storm Distance: " + String.valueOf(nearestStormDistance) + "\n" +
               "Nearest Storm Bearing: " + String.valueOf(nearestStormBearing) + "\n" +
               "Precipitation Intensity: " + String.valueOf(precipIntensity) + "\n" +
               "Precipitation Probability: " + String.valueOf(precipProbability) + "\n" +
               "Temperature: " + String.valueOf(temperature) + "\n" +
               "Apparent Temperature: " + String.valueOf(apparentTemperature) + "\n" +
               "Dew Point: " + String.valueOf(dewPoint) + "\n" +
               "Humidity: " + String.valueOf(humidity) + "\n" +
               "Wind Speed: " + String.valueOf(windSpeed) + "\n" +
               "Wind Bearing: " + String.valueOf(windBearing) + "\n" +
               "Visibility: " + String.valueOf(visibility) + "\n" +
               "Cloud Cover: " + String.valueOf(cloudCover) + "\n" +
               "Pressure: " + String.valueOf(pressure) + "\n" +
               "Ozone: " + String.valueOf(ozone);
    }
}
