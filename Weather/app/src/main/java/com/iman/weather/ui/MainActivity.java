package com.iman.weather.ui;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.iman.weather.R;
import com.iman.weather.data.WeatherData;
import com.iman.weather.service.ForecastService;

public class MainActivity extends AppCompatActivity {
    private static final long  LOCATION_REFRESH_TIME     = 60000;
    private static final float LOCATION_REFRESH_DISTANCE = 1000;
    private static final int   PERMISSION_ID             = 1;

    private TextView textView;
    private WeatherData weatherData = null;
    private LocationManager  mLocationManager;
    private LocationListener mLocationListener;
    private int currentDataView = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayShowTitleEnabled(false);
        }

        textView = (TextView) findViewById(R.id.data);
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        if ((spinner != null) && (toolbar != null)) {
            spinner.setAdapter(new MyAdapter(
                    toolbar.getContext(),
                    new String[] {
                            "Location Information",
                            "Current Weather",
                            "Minutely Forecast",
                            "Hourly Forecast",
                            "Daily Forecast",
                            "Alerts",
                            "Flags"
                    }));

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    currentDataView = position;
                    updateData();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                    textView.setText("Select a page");
                }
            });
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    loadWeatherFromLastKnownLocation();
                }
            });
        }

        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                loadWeatherData(location.getLatitude(), location.getLongitude());
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!checkLocationPermission(true)) {
            return;
        }

        Toast.makeText(this, "Requesting location...", Toast.LENGTH_SHORT).show();
        loadWeatherFromLastKnownLocation();
        mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, LOCATION_REFRESH_TIME,
                                                LOCATION_REFRESH_DISTANCE, mLocationListener);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (checkLocationPermission(false)) {
            mLocationManager.removeUpdates(mLocationListener);
        }
    }

    private boolean checkLocationPermission(Boolean canAsk) {
        if (!hasPermission(Manifest.permission.ACCESS_FINE_LOCATION)) {
            if (canAsk)
                requestPermission(Manifest.permission.ACCESS_FINE_LOCATION, "Please allow location access", PERMISSION_ID);
            return false;
        }
        return true;
    }

    private void loadWeatherFromLastKnownLocation() {
        if (!checkLocationPermission(false)) {
            return;
        }

        Criteria criteria     = new Criteria();
        String   bestProvider = mLocationManager.getBestProvider(criteria, true);
        Location location     = mLocationManager.getLastKnownLocation(bestProvider);
        if (location != null)
            loadWeatherData(location.getLatitude(), location.getLongitude());
        else
            Toast.makeText(this, "Waiting for location to be determined...", Toast.LENGTH_LONG).show();
    }

    private void loadWeatherData(double lat, double lon) {
        ForecastService forecastService = new ForecastService();
        forecastService.addObserver(new ForecastService.Callback() {
            @Override
            public void onComplete(WeatherData weatherData) {
                MainActivity.this.weatherData = weatherData;
                Toast.makeText(MainActivity.this, "Data refreshed", Toast.LENGTH_LONG).show();
                updateData();
            }

            @Override
            public void onFailure(String reason) {
                final AlertDialog.Builder providerDialog = new AlertDialog.Builder(MainActivity.this);
                providerDialog.setTitle(":-(").setMessage(reason).setPositiveButton("OK", null).show();
            }
        }).execute(
                //27.195284, -71.523942
                lat, lon
        );
    }

    private void updateData() {
        String s = "";
        if (weatherData != null) {
            switch (currentDataView) {
                case 0:
                    s = weatherData.toString();
                    break;
                case 1:
                    s = weatherData.currently != null ? weatherData.currently.toString() : "Data not available!";
                    break;
                case 2:
                    s = weatherData.minutely != null ? weatherData.minutely.toString() : "Minutely data not available!";
                    break;
                case 3:
                    s = weatherData.hourly != null ? weatherData.hourly.toString() : "Hourly data not available!";
                    break;
                case 4:
                    s = weatherData.daily != null ? weatherData.daily.toString() : "Daily data not available!";
                    break;
                case 5:
                    s = weatherData.alertsToString();
                    break;
                case 6:
                    s = weatherData.flags != null ? weatherData.flags.toString() : "No flags!";
                    break;
            }
        }
        textView.setText(s);
    }

    private boolean hasPermission(String permission) {
        boolean result = Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP;
        if (!result) {
            int permissionCheckResult = ActivityCompat.checkSelfPermission(this, permission);
            if (permissionCheckResult == PackageManager.PERMISSION_GRANTED) {
                result = true;
            }
        }
        return result;
    }

    private void requestPermission(String permission, String description, int permissionID) {
        try {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, permission)) {
                Toast.makeText(this, description, Toast.LENGTH_LONG).show();
            } else {
                ActivityCompat.requestPermissions(this, new String[] {permission}, permissionID);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static class MyAdapter extends ArrayAdapter<String> implements ThemedSpinnerAdapter {
        private final ThemedSpinnerAdapter.Helper mDropDownHelper;

        public MyAdapter(Context context, String[] objects) {
            super(context, android.R.layout.simple_list_item_1, objects);
            mDropDownHelper = new ThemedSpinnerAdapter.Helper(context);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            View view;

            if (convertView == null) {
                LayoutInflater inflater = mDropDownHelper.getDropDownViewInflater();
                view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
            } else {
                view = convertView;
            }

            TextView textView = (TextView) view.findViewById(android.R.id.text1);
            textView.setText(getItem(position));

            return view;
        }

        @Override
        public void setDropDownViewTheme(Resources.Theme theme) {
            mDropDownHelper.setDropDownViewTheme(theme);
        }

        @Override
        public Resources.Theme getDropDownViewTheme() {
            return mDropDownHelper.getDropDownViewTheme();
        }
    }

}
