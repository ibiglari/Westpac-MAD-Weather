package com.iman.weather.data;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class WeatherData {

    public double    latitude;
    public double    longitude;
    public String    timezone;
    public long      offset;
    public Currently currently;
    public Minutely  minutely;
    public Hourly    hourly;
    public Daily     daily;
    public List<AlertDatum> alerts = new ArrayList<>();
    public Flags     flags;

    public String toString() {
        String ret = "Latitude: " + String.valueOf(latitude) + "\n" +
                     "Longitude: " + String.valueOf(longitude) + "\n" +
                     "Time zone: " + timezone + "\n" +
                     "Offset: " + String.valueOf(offset);
        return ret;
    }

    public String alertsToString() {
        String ret = "";
        for (int i = 0; i < alerts.size(); i++) {
            AlertDatum alert = alerts.get(i);
            ret += "\n" + alert.toString() + "\n";
        }
        return alerts.size() > 0 ? ret : "No alerts!\n";
    }

    public static WeatherData getWeatherData(String json) {
        Gson gson = new Gson();
        WeatherData weatherData = gson.fromJson(json, WeatherData.class);
        assert(weatherData != null);
        return weatherData;
    }
}
