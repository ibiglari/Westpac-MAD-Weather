
package com.iman.weather.data;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DailyDatum {

    public long   time;
    public String summary;
    public String icon;
    public long   sunriseTime;
    public long   sunsetTime;
    public double moonPhase;
    public double precipIntensity;
    public double precipIntensityMax;
    public double precipProbability;
    public double temperatureMin;
    public long   temperatureMinTime;
    public double temperatureMax;
    public long   temperatureMaxTime;
    public double apparentTemperatureMin;
    public long   apparentTemperatureMinTime;
    public double apparentTemperatureMax;
    public long   apparentTemperatureMaxTime;
    public double dewPoint;
    public double humidity;
    public double windSpeed;
    public double windBearing;
    public double visibility;
    public double cloudCover;
    public double pressure;
    public double ozone;

    public String toString() {
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        return "Day: " + (new SimpleDateFormat("EEEE MM/dd")).format(new Date(time * 1000)) + "\n" +
               "Summary: " + summary + "\n" +
               "Icon: " + icon + "\n" +
               "Sunrise: " + sdf.format(new Date(sunriseTime * 1000)) + "\n" +
               "Sunset: " + sdf.format(new Date(sunsetTime * 1000)) + "\n" +
               "Moon Phase: " + String.valueOf(moonPhase) + "\n" +
               "Precipitation Intensity: " + String.valueOf(precipIntensity) + "\n" +
               "Max Precipitation Intensity: " + String.valueOf(precipIntensityMax) + "\n" +
               "Precipitation Probability: " + String.valueOf(precipProbability) + "\n" +
               "Min Temperature: " + String.valueOf(temperatureMin) + "\n" +
               "Min Temperature Time: " + sdf.format(new Date(temperatureMinTime)) + "\n" +
               "Max Temperature: " + String.valueOf(temperatureMax) + "\n" +
               "Max Temperature Time: " + sdf.format(new Date(temperatureMaxTime)) + "\n" +
               "Min Apparent Temperature: " + String.valueOf(apparentTemperatureMin) + "\n" +
               "Min Apparent Temperature Time: " + sdf.format(new Date(apparentTemperatureMinTime)) + "\n" +
               "Max Apparent Temperature: " + String.valueOf(apparentTemperatureMax) + "\n" +
               "Max Apparent Temperature Time: " + sdf.format(new Date(apparentTemperatureMaxTime)) + "\n" +
               "Dew Point: " + String.valueOf(dewPoint) + "\n" +
               "Humidity: " + String.valueOf(humidity) + "\n" +
               "Wind Speed: " + String.valueOf(windSpeed) + "\n" +
               "Wind Bearing: " + String.valueOf(windBearing) + "\n" +
               "Visibility: " + String.valueOf(visibility) + "\n" +
               "Cloud Cover: " + String.valueOf(cloudCover) + "\n" +
               "Pressure: " + String.valueOf(pressure) + "\n" +
               "Ozone: " + String.valueOf(ozone);
    }
}
