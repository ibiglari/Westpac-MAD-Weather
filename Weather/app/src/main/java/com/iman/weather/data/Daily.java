
package com.iman.weather.data;

import java.util.ArrayList;
import java.util.List;

public class Daily {

    public String summary;
    public String icon;
    public List<DailyDatum> data = new ArrayList<>();

    public String toString() {
        String ret = "Summary: " + summary + "\n" +
                     "Icon: " + icon + "\n";

        for (int i = 0; i < data.size(); i++) {
            DailyDatum day = data.get(i);
            ret += "\n" + day.toString() + "\n";
        }
        return ret;
    }
}
