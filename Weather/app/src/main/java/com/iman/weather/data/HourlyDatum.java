
package com.iman.weather.data;

import java.text.SimpleDateFormat;
import java.util.Date;

public class HourlyDatum {

    public long   time;
    public String summary;
    public String icon;
    public double precipIntensity;
    public double precipProbability;
    public double temperature;
    public double apparentTemperature;
    public double dewPoint;
    public double humidity;
    public double windSpeed;
    public double windBearing;
    public double visibility;
    public double cloudCover;
    public double pressure;
    public double ozone;

    public String toString() {
        String ret = "Time: " + (new SimpleDateFormat("EEEE HH:mm")).format(new Date(time * 1000)) + "\n" +
                     "Summary: " + summary + "\n" +
                     "Icon: " + icon + "\n" +
                     "Precipitation Intensity: " + String.valueOf(precipIntensity) + "\n" +
                     "Precipitation Probability: " + String.valueOf(precipProbability) + "\n" +
                     "Temperature: " + String.valueOf(temperature) + "\n" +
                     "Apparent Temperature: " + String.valueOf(apparentTemperature) + "\n" +
                     "Dew Point: " + String.valueOf(dewPoint) + "\n" +
                     "Humidity: " + String.valueOf(humidity) + "\n" +
                     "Wind Speed: " + String.valueOf(windSpeed) + "\n" +
                     "Wind Bearing: " + String.valueOf(windBearing) + "\n" +
                     "Visibility: " + String.valueOf(visibility) + "\n" +
                     "Cloud Cover: " + String.valueOf(cloudCover) + "\n" +
                     "Pressure: " + String.valueOf(pressure) + "\n" +
                     "Ozone: " + String.valueOf(ozone);
        return ret;
    }
}
