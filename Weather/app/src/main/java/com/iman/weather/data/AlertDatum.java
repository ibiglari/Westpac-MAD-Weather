package com.iman.weather.data;

import java.util.Date;

public class AlertDatum {
    public String title;
    public String description;
    public long expires;
    public String uri;

    public String toString() {
        return "Title: " + title + "\n" +
               "Description: " + description + "\n" +
               "Expires On: " + String.valueOf(new Date(expires * 1000)) + "\n" +
               "More Information: " + uri;
    }
}
